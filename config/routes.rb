# coding: utf-8
Rails.application.routes.draw do

  devise_for :users
  #page d'accueil
  get '' => 'default#home', as: 'home'

  # affiche l utilisateur courrant et sa liste d amis
  get 'users/show' => 'user#show_user', as: 'show_user'


  #affiche la liste complete des utilisateurs
  get 'users/list' => 'user#list_user', as: 'list_user'

  #ajoute utilisateur en tant qu ami si le lien n existai toujours pas
  get 'users/add_friend/:id' => 'user#add_friend', as: 'add_friend', :constraints => { :id => /\d+/ }

  #warning si on tent d ajouter 2 fois le meme utilisateur dans sa liste d amis
  get 'users/already_friend' => 'user#already_friend', as: 'already_friend'
  
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
