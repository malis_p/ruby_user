class Friend < ActiveRecord::Base
  belongs_to :target, class_name: "User"
  belongs_to :source, class_name: "User"
  validates :target_id, presence: true
  validates :source_id, presence: true
end
