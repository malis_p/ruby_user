class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :active_friends, class_name: "Friend",
  foreign_key: "target_id",
  dependent: :destroy


  has_many :is_friends, through: :active_friends, source: :source

  
end
