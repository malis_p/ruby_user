# coding: utf-8
class UserController < ApplicationController

  def show_user
    #oblige l utilisateur a etre connecté
    @user = current_user
    if @user.blank?
      redirect_to home_path
    end

    #recupere la liste d association
    @friend_list = ::Friend.where(source_id: @user.id)
    @user_list = []
    @friend_list.each do |friend|
      # pour chaque association, on recupere avec qui elle est faite
      @user_list.push( User.find_by id: friend.target_id )
    end
    
  end


  def list_user
    @users = ::User.all
  end

  
  def add_friend
    #oblige l utilisateur a etre connecté
    @user = current_user
    if @user.blank?
      redirect_to home_path
    end

    # on essaye de recuperer avec qui on veut etre amis, sinon on est redirigé sur home
    @target_id = params[:id]
    @target_user = User.find_by id: @target_id
    if @target_user.blank?
      redirect_to home_path
    end

    # est on deja ami avec cette personne ? si oui redirection si non on créer la relation
    @friend = Friend.find_by source_id: @user.id, target_id: @target_user.id
    if @friend.blank?
      Friend.create(source_id: @user.id, target_id: @target_user.id)
      redirect_to show_user_path
    else
      redirect_to already_friend_path
    end
  end
  
  def already_friend
  end
  
end
