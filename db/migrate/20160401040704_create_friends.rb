class CreateFriends < ActiveRecord::Migration
  def change
    create_table :friends do |t|
      t.integer :source_id
      t.integer :target_id

      t.timestamps null: false
    end

    add_index :friends, :source_id
    add_index :friends, :target_id
    add_index :friends, [:target_id, :source_id], unique: true
    
  end
end
